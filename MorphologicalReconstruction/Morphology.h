#pragma once

#include <opencv2/core/core.hpp>

namespace tj
{
	enum Connectivity 
	{
		FOUR_CONNECTED  = 4,
		EIGHT_CONNECTED = 8
	};

	cv::Mat GeodesicDilate(cv::Mat marker, cv::Mat mask, Connectivity conn = EIGHT_CONNECTED);

	cv::Mat GeodesicErode(cv::Mat marker, cv::Mat mask, Connectivity conn = EIGHT_CONNECTED);

	cv::Mat Reconstruct(cv::Mat marker, cv::Mat mask, Connectivity conn = EIGHT_CONNECTED);
}