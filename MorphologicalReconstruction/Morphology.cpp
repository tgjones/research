#include <stdexcept>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

cv::Mat tj::GeodesicDilate(cv::Mat marker, cv::Mat mask, tj::Connectivity conn)
{
	// Create structuring element
	cv::Mat element = cv::Mat::zeros(3, 3, CV_8U);
	if (conn == EIGHT_CONNECTED)
		element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
	else if (conn == FOUR_CONNECTED)
		element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(3, 3));
	else
		throw std::runtime_error("Unsupported connectivity, only 4 or 8 supported");

	// Do dilation
	cv::Mat dilated = cv::Mat::zeros(marker.size(), marker.type());
	cv::dilate(marker, dilated, element);

	// Intersect and return
	return cv::min(dilated, mask);
}

cv::Mat tj::GeodesicErode(cv::Mat marker, cv::Mat mask, tj::Connectivity conn)
{
	// Create structuring element
	cv::Mat element = cv::Mat::zeros(3, 3, CV_8U);
	if (conn == EIGHT_CONNECTED)
		element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
	else if (conn == FOUR_CONNECTED)
		element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(3, 3));
	else
		throw std::runtime_error("Unsupported connectivity, only 4 or 8 supported");

	// Do erosion
	cv::Mat eroded = cv::Mat::zeros(marker.size(), marker.type());
	cv::erode(marker, eroded, element);

	// Union and return
	return cv::max(eroded, mask);
}

cv::Mat tj::Reconstruct(cv::Mat marker, cv::Mat mask, tj::Connectivity conn)
{
	cv::Mat m0, m1 = marker;
	do
	{
		m0 = m1.clone();
		m1 = GeodesicDilate(m0, mask, conn);
	} 
	while (!IsEqual(m0, m1));
	return m1;
}
