import cv2
import pandas

# This script prepares the test dataset and relies on my PhD dataset being somewhere else on disk

data_root = 'C:\Images\ShoeMarkTimeSeries'


def wear_data(subject):

    if subject < 4:
        return [pandas.read_excel(data_root + '/gnd/Subject' + str(subject) + '.xlsx', '01 (Ref)'),
                pandas.read_excel(data_root + '/gnd/Subject' + str(subject) + '.xlsx', '02'),
                pandas.read_excel(data_root + '/gnd/Subject' + str(subject) + '.xlsx', '03'),
                pandas.read_excel(data_root + '/gnd/Subject' + str(subject) + '.xlsx', '04'),
                pandas.read_excel(data_root + '/gnd/Subject' + str(subject) + '.xlsx', '05'),
                pandas.read_excel(data_root + '/gnd/Subject' + str(subject) + '.xlsx', '06'),
                pandas.read_excel(data_root + '/gnd/Subject' + str(subject) + '.xlsx', '07')]

    elif subject == 4:
        return [pandas.read_excel(data_root + '/gnd/Subject' + str(subject) + '.xlsx', '01 (Ref)'),
                pandas.read_excel(data_root + '/gnd/Subject' + str(subject) + '.xlsx', '02'),
                pandas.read_excel(data_root + '/gnd/Subject' + str(subject) + '.xlsx', '03'),
                pandas.read_excel(data_root + '/gnd/Subject' + str(subject) + '.xlsx', '04'),
                pandas.read_excel(data_root + '/gnd/Subject' + str(subject) + '.xlsx', '05')]

    else:
        raise IndexError('Invalid subject: ' + str(subject))


def image_pair(subject, time_slot):

    if subject == 1:
        if time_slot == 0:
            return (cv2.imread(data_root + '/registered/Subject1/01_Subject1_0km_140610_0001.png'),
                    cv2.imread(data_root + '/registered/Subject1/01_Subject1_0km_140610_0002.png'))
        elif time_slot == 1:
            return (cv2.imread(data_root + '/registered/Subject1/02_Subject1_55km_190710_0001.png'),
                    cv2.imread(data_root + '/registered/Subject1/02_Subject1_55km_190710_0002.png'))
        elif time_slot == 2:
            return (cv2.imread(data_root + '/registered/Subject1/03_Subject1_32km_020810_0001.png'),
                    cv2.imread(data_root + '/registered/Subject1/03_Subject1_32km_020810_0002.png'))
        elif time_slot == 3:
            return (cv2.imread(data_root + '/registered/Subject1/04_Subject1_35km_060910_0001.png'),
                    cv2.imread(data_root + '/registered/Subject1/04_Subject1_35km_060910_0002.png'))
        elif time_slot == 4:
            return (cv2.imread(data_root + '/registered/Subject1/05_Subject1_10km_200910_0001.png'),
                    cv2.imread(data_root + '/registered/Subject1/05_Subject1_10km_200910_0002.png'))
        elif time_slot == 5:
            return (cv2.imread(data_root + '/registered/Subject1/06_Subject1_12km_041010_0001.png'),
                    cv2.imread(data_root + '/registered/Subject1/06_Subject1_12km_041010_0002.png'))
        elif time_slot == 6:
            return (cv2.imread(data_root + '/registered/Subject1/07_Subject1_35km_181010_0001.png'),
                    cv2.imread(data_root + '/registered/Subject1/07_Subject1_35km_181010_0002.png'))
        else:
            raise IndexError('Invalid time slot: ' + str(time_slot))

    elif subject == 2:
        if time_slot == 0:
            return (cv2.imread(data_root + '/registered/Subject2/01_Subject2_0km_140610_0001.png'),
                    cv2.imread(data_root + '/registered/Subject2/01_Subject2_0km_140610_0002.png'))
        elif time_slot == 1:
            return (cv2.imread(data_root + '/registered/Subject2/02_Subject2_32km_280610_0001.png'),
                    cv2.imread(data_root + '/registered/Subject2/02_Subject2_32km_280610_0002.png'))
        elif time_slot == 2:
            return (cv2.imread(data_root + '/registered/Subject2/03_Subject2_53km_180710_0001.png'),
                    cv2.imread(data_root + '/registered/Subject2/03_Subject2_53km_180710_0002.png'))
        elif time_slot == 3:
            return (cv2.imread(data_root + '/registered/Subject2/04_Subject2_76km_150810_0001.png'),
                    cv2.imread(data_root + '/registered/Subject2/04_Subject2_76km_150810_0002.png'))
        elif time_slot == 4:
            return (cv2.imread(data_root + '/registered/Subject2/05_Subject2_94km_210910_0001.png'),
                    cv2.imread(data_root + '/registered/Subject2/05_Subject2_94km_210910_0002.png'))
        elif time_slot == 5:
            return (cv2.imread(data_root + '/registered/Subject2/06_Subject2_45miles_041010_0001.png'),
                    cv2.imread(data_root + '/registered/Subject2/06_Subject2_45miles_041010_0002.png'))
        elif time_slot == 6:
            return (cv2.imread(data_root + '/registered/Subject2/07_Subject2_47miles_181010_0001.png'),
                    cv2.imread(data_root + '/registered/Subject2/07_Subject2_47miles_181010_0002.png'))
        else:
            raise IndexError('Invalid time slot: ' + str(time_slot))

    elif subject == 3:
        if time_slot == 0:
            return (cv2.imread(data_root + '/registered/Subject3/01_Subject3_0miles_140610_0001.png'),
                    cv2.imread(data_root + '/registered/Subject3/01_Subject3_0miles_140610_0002.png'))
        elif time_slot == 1:
            return (cv2.imread(data_root + '/registered/Subject3/02_Subject3_48miles_290610_0001.png'),
                    cv2.imread(data_root + '/registered/Subject3/02_Subject3_48miles_290610_0002.png'))
        elif time_slot == 2:
            return (cv2.imread(data_root + '/registered/Subject3/03_Subject3_10miles_050710_0001.png'),
                    cv2.imread(data_root + '/registered/Subject3/03_Subject3_10miles_050710_0002.png'))
        elif time_slot == 3:
            return (cv2.imread(data_root + '/registered/Subject3/04_Subject3_92miles_150810_0001.png'),
                    cv2.imread(data_root + '/registered/Subject3/04_Subject3_92miles_150810_0002.png'))
        elif time_slot == 4:
            return (cv2.imread(data_root + '/registered/Subject3/05_Subject3_71miles_200910_0001.png'),
                    cv2.imread(data_root + '/registered/Subject3/05_Subject3_71miles_200910_0002.png'))
        elif time_slot == 5:
            return (cv2.imread(data_root + '/registered/Subject3/06_Subject3_25miles_041010_0001.png'),
                    cv2.imread(data_root + '/registered/Subject3/06_Subject3_25miles_041010_0002.png'))
        elif time_slot == 6:
            return (cv2.imread(data_root + '/registered/Subject3/07_Subject3_43miles_181010_0001.png'),
                    cv2.imread(data_root + '/registered/Subject3/07_Subject3_43miles_181010_0002.png'))
        else:
            raise IndexError('Invalid time slot: ' + str(time_slot))

    elif subject == 4:
        if time_slot == 0:
            return (cv2.imread(data_root + '/registered/Subject4/01_Subject4_0_050710_0001.png'),
                    cv2.imread(data_root + '/registered/Subject4/01_Subject4_0_050710_0002.png'))
        elif time_slot == 1:
            return (cv2.imread(data_root + '/registered/Subject4/02_Subject4_18miles_190710_0001.png'),
                    cv2.imread(data_root + '/registered/Subject4/02_Subject4_18miles_190710_0002.png'))
        elif time_slot == 2:
            return (cv2.imread(data_root + '/registered/Subject4/03_Subject4_99miles_200910_0001.png'),
                    cv2.imread(data_root + '/registered/Subject4/03_Subject4_99miles_200910_0002.png'))
        elif time_slot == 3:
            return (cv2.imread(data_root + '/registered/Subject4/04_Subject4_66miles_181010_0001.png'),
                    cv2.imread(data_root + '/registered/Subject4/04_Subject4_66miles_181010_0002.png'))
        elif time_slot == 4:
            return (cv2.imread(data_root + '/registered/Subject4/05_Subject4_32miles_011110_0001.png'),
                    cv2.imread(data_root + '/registered/Subject4/05_Subject4_32miles_011110_0001.png'))
        else:
            raise IndexError('Invalid time slot: ' + str(time_slot))
    
        
def time_series(subject, block):
    series = []
    for t in xrange(7 if subject < 4 else 5):
        img, _ = image_pair(subject, t)
        if img is None:
            raise RuntimeError('Error reading dataset image')
        series.append(cv2.cvtColor(img[block[0]*200:(block[0]*200)+200, block[1]*200:(block[1]*200)+200],
                                   cv2.COLOR_BGR2GRAY))
    return series
