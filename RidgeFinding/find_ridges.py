import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt


def show_grayscale_image(img, title=''):
    plt.imshow(img, cmap=plt.get_cmap('gray'))
    plt.axis('off')
    plt.title(title)


def detect_ridges(psd, knl):
    ridges = psd.copy()
    ridges = cv.filter2D(ridges, -1, knl)
    _, ridges = cv.threshold(ridges, 250, 255, cv.THRESH_BINARY)
    contours, _ = cv.findContours(ridges, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    ridges = np.zeros(ridges.shape, np.uint8)
    for contour in contours:
        if cv.contourArea(contour) > 20:
            cv.drawContours(ridges, [contour], 0, 255)
    return ridges


def main():
    cc = np.array([[1, 1, 1, 1, 1],
               [1, 100, 100, 100, 1],
               [1, 100, 1000, 100, 1],
               [1, 100, 100, 100, 1],
               [1, 1, 1, 1, 1]])

    w = np.array([

        # 11.25 deg
        [[-6.0, -1.0, 4.0,  4.0, -1.0],
         [-6.0,  1.5, 4.0,  4.0, -3.5],
         [-6.0,  4.0, 4.0,  4.0, -6.0],
         [-3.5,  4.0, 4.0,  1.5, -6.0],
         [-1.0,  4.0, 4.0, -1.0, -6.0]],

        # 22.5 deg

        # 33.75 deg

        # 45 deg

        # 56.25 deg

        # 67.5 deg

        # 78.75 deg

        # 90 deg  # [[-6.0,-6.0,-6.0,-6.0,-6.0],   # [ 4.0, 4.0, 4.0, 4.0, 4.0],  #  [ 4.0, 4.0, 4.0, 4.0, 4.0],
                    #  [ 4.0, 4.0, 4.0, 4.0, 4.0],  #  [-6.0,-6.0,-6.0,-6.0,-6.0]],  # 101.25 deg  # 112.5 deg  # 123.75 deg
                    # 135 deg     	  # 146.25 deg  # 157.5 deg  # 168.75 deg  # 180 deg
    ])

    psd_high = cv.imread('psd_high_wear.png', cv.CV_LOAD_IMAGE_GRAYSCALE)
    psd_medium = cv.imread('psd_medium_wear.png', cv.CV_LOAD_IMAGE_GRAYSCALE)

    fig = plt.figure()
    fig.canvas.set_window_title('Ridge Detection')
    fig.add_subplot(221)

    plt.subplot(221)
    show_grayscale_image(psd_medium, 'PSD medium wear')

    plt.subplot(222)
    show_grayscale_image(psd_high, 'PSD high wear')

    for knl in w:

        ridges_high = detect_ridges(psd_high, knl)
        ridges_medium = detect_ridges(psd_medium, knl)

        plt.subplot(223)
        show_grayscale_image(ridges_medium, 'Ridges medium wear')

        plt.subplot(224)
        show_grayscale_image(ridges_high, 'Ridges high wear')

    plt.show()

if __name__ == '__main__':
    main()



# Danny's MATLAB code
#
# for v = 1:1    %  Should be 16 eventually!
# % Calculate image filtered by w(v)
#     for m = 1:k-4
#         for n = 1:k-4
#             % do convolution with mask in direction v (using w(v); result in dir(v))
#             % Mask is w(v,i,j) = w((v-1)*5+i, j)
#             temp = 0.0;
#             for i=1:5
#                 for j=1:5
#                     pixel = double(in(m+i-1, n+j-1));
#                     wt = w((v-1)*5 + i, j);
#                     temp = temp + pixel * wt;
#                 end
#             end
#             %temp = abs(temp); - if a ridge line, must be +ve, so no abs
#             if (temp < threshold)
#                 temp = 0.0;
#             else  % for now, make binary
#                 temp = 1.0;
#             end
#             dir((v-1)*k+m+2,n+2) = temp;
#             % dir1(v, m+2,n+2) = temp;  % This is what I wanted to write
#         end
#     end
#     imshow(dir((v-1)*k+1:v*k,1:k));

#     % The better method of tidying up small fragments/noise:
#     % Remove isolated segments of, say, three pixels or less
#     % Find all connected regions, and remove those whose size is <= 3
#     % connected = bwconncomp(dir(1:k, 1:k));
#     % pps = connected.PixelIdxList;
#     % ppslen = size(pps, 2);   % = number of regions
#     % Remove an item from pps if it has 3 or fewer elements
#     % Leave result in image c
#     % imshow(c(1:k, 1:k);
#     % copy c into dir

#     % For now, this removes short segments up to two pixels in length

#     c = zeros(1:k, 1:k);
#     for m = 1:k-4
#         for n = 1:k-4
#             % do convolution with circle mask, result in c
#             temp1 = 0.0;
#             for i=1:5
#                 for j=1:5
#                     pixel = dir(m+i-1, n+j-1);
#                     wt = cc(i,j);
#                     temp1 = temp1 + pixel * wt;
#                 end
#             end
#             if (temp1 >= 1000) % central pixel is 1
#                 if (temp1 < 1100)  % no inner ring - centre is isolated
#                     pix = 0;
#                 else
#                     if (temp1 < 1200)  % only one in inner ring
#                         % check if any outer ring
#                         temp1 = mod(temp1, 100);  % = sigma outer ring
#                         if (temp1 == 0)
#                             pix = 0;
#                         else 
#                             pix = 1;
#                         end
#                     else
#                         pix = 1;
#                     end
#                 end
#             else
#                 pix = 0;
#             end
#             c(m+2,n+2) = pix;
#         end
#     end
#     imshow(c);  % shows the result of 
# end


# % Now select the best one of the 16 directional responses 
# % Not yet implemented
