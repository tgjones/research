import sys
import math
import cv2 as cv
import numpy as np

stain_vectors = {

    'H&E': np.array([
        [0.644211, 0.716556, 0.266844],
        [0.092789, 0.954111, 0.283111],
        [0.0, 0.0, 0.0]]),

    'H&E 2': np.array([
        [0.49015734, 0.76897085, 0.41040173],
        [0.04615336, 0.8420684,  0.5373925],
        [0.0, 0.0, 0.0]]),

    'H DAB': np.array([
        [0.650, 0.704, 0.286],
        [0.268, 0.570, 0.776],
        [0.0, 0.0, 0.0]]),

    'Feulgen Light Green': np.array([
        [0.46420921, 0.83008335, 0.30827187],
        [0.94705542, 0.25373821, 0.19650764],
        [0.0, 0.0, 0.0]]),

    'Giemsa': np.array([
        [0.834750233, 0.513556283, 0.196330403],
        [0.092789, 0.954111, 0.283111],
        [0.0, 0.0, 0.0]]),

    'FastRed FastBlue DAB': np.array([
        [0.21393921, 0.85112669, 0.47794022],
        [0.74890292, 0.60624161, 0.26731082],
        [0.268, 0.570, 0.776]]),

    'Methyl Green DAB': np.array([
        [0.98003, 0.144316, 0.133146],
        [0.268, 0.570, 0.776],
        [0.0, 0.0, 0.0]]),

    'H&E DAB': np.array([
        [0.650, 0.704, 0.286],
        [0.072, 0.990, 0.105],
        [0.268, 0.570, 0.776]]),

    'H AEC': np.array([
        [0.650, 0.704, 0.286],
        [0.2743, 0.6796, 0.6803],
        [0.0, 0.0, 0.0]]),

    'Azan-Mallory': np.array([
        [0.853033, 0.508733, 0.112656],
        [0.09289875, 0.8662008, 0.49098468],
        [0.10732849, 0.36765403, 0.9237484]]),

    'Masson Trichrome': np.array([
        [0.7995107, 0.5913521, 0.10528667],
        [0.09997159, 0.73738605, 0.6680326],
        [0.0, 0.0, 0.0]]),

    'Alcian blue & H': np.array([
        [0.874622, 0.457711, 0.158256],
        [0.552556, 0.7544, 0.353744],
        [0.0, 0.0, 0.0]]),

    'H PAS': np.array([
        [0.644211, 0.716556, 0.266844],
        [0.175411, 0.972178, 0.154589],
        [0.0, 0.0, 0.0]]),

    'RGB': np.array([
        [0.0, 1.0, 1.0],
        [1.0, 0.0, 1.0],
        [1.0, 1.0, 0.0]]),

    'CMY': np.array([
        [1.0, 0.0, 0.0],
        [0.0, 1.0, 0.0],
        [0.0, 0.0, 1.0]])

}


def deconvolve_stain(I, stain_name):

    if I.shape[2] != 3:
        raise RuntimeError('RGB image required')

    # Get stain vector
    try:
        C = stain_vectors[stain_name]
    except IndexError:
        raise RuntimeError("Stain '" + stain_name + "' supported")

    # Normalise vector lengths
    for dim in C:
        len = math.sqrt((dim[0] * dim[0]) + (dim[1] * dim[1]) + (dim[2] * dim[2]))
        if len != 0:
            dim /= len

    # Check colour vector 1 is nonzero
    if np.count_nonzero(C[0]) == 0:
        raise RuntimeError("Stain matrix needs at least one colour vector in the first row")

    # Check colour vector 2 is nonzero (copy from vector 1)
    if np.count_nonzero(C[1]) == 0:
        C[1] = C[0]

    # Check colour vector 3 is nonzero (calculate magnitude remainder)
    if np.count_nonzero(C[2]) == 0:
        for x in xrange(3):
            C[2][x] = math.sqrt(1.0 - C[0][x]*C[0][x] - C[1][x]*C[1][x])

    # Invert
    D = np.linalg.inv(C)

    # Convert to OD space
    M = I.astype(np.float32)
    log255 = math.log(255)
    for row in M:
        for pxl in row:
            od = -(255. * np.log((pxl + 1.) / 255.) / log255)
            for x in xrange(3):
                scaled = od*D[x]
                pxl[x] = math.exp(-(np.sum(scaled) - 255.) * log255 / 255.)

    # Clamp values and return integer view
    M = np.clip(M, 0, 255)
    return M.astype(np.uint8)

if __name__ == '__main__':
    I = cv.imread(sys.argv[1])
    deconvolve_stain(I, 'H DAB')