import sys
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt


def dft_numpy(input_file, output_file=None):
    """ Discrete Fourier Transform (DFT) using NumPy """

    # Load input image
    img = cv.imread(input_file, flags=cv.CV_LOAD_IMAGE_GRAYSCALE)
    if img is None:
        raise SystemError("Could not read image file '" + input_file + "'")

    # Do DFT
    dft = np.fft.fft2(img)
    dft = np.fft.fftshift(dft)
    mag = np.log(np.abs(dft))

    if __debug__:
        plt.subplot(121), plt.imshow(img, cmap='gray')
        plt.title('Input Image'), plt.xticks([]), plt.yticks([])
        plt.subplot(122), plt.imshow(mag, cmap='gray')
        plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
        plt.show()

    if output_file is not None:
        cv.imwrite(output_file, mag)

    return mag


def dft_opencv(input_file, output_file=None):
    """ Discrete Fourier Transform (DFT) using OpenCV 2 """

    # Load input image
    img = cv.imread(input_file, flags=cv.CV_LOAD_IMAGE_GRAYSCALE)
    if img is None:
        raise SystemError("Could not read image file '" + input_file + "'")

    # Expand to optimal size for DFT
    m = cv.getOptimalDFTSize(img.shape[0])
    n = cv.getOptimalDFTSize(img.shape[1])
    padded = cv.copyMakeBorder(img, 0, m - img.shape[0], 0, n - img.shape[1], cv.BORDER_CONSTANT)

    # Do DFT
    dft = cv.dft(np.float32(padded), flags=cv.DFT_COMPLEX_OUTPUT)
    dft = np.fft.fftshift(dft)

    # Compute magnitude and switch to logarithmic scale
    mag = np.log(cv.magnitude(dft[:, :, 0], dft[:, :, 1]))

    if __debug__:
        plt.subplot(121), plt.imshow(img, cmap='gray')
        plt.title('Input Image'), plt.xticks([]), plt.yticks([])
        plt.subplot(122), plt.imshow(mag, cmap='gray')
        plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
        plt.show()

    if output_file is not None:
        cv.imwrite(output_file, mag)

    return mag


if __name__ == '__main__':
    dft_opencv(str(sys.argv[1]))
