import sys
import math
import cv2 as cv
import numpy as np


def mean_angle(img):

    I = cv.imread(img, cv.CV_LOAD_IMAGE_GRAYSCALE)
    I = cv.GaussianBlur(I, (3, 3), 0)

    # Sobel kernels
    Gx = np.array([[-1, 0, +1],
                   [-2, 0, +2],
                   [-1, 0, +1]])

    Gy = np.array([[-1, -2, -1],
                   [0,   0,  0],
                   [+1, +2, +1]])

    # Convolve with Sobel kernels
    Ix = cv.filter2D(I, -1, Gx)
    Iy = cv.filter2D(I, -1, Gy)

    # Edge gradient and pixel angle
    G = np.sqrt((Ix*Ix)+(Iy*Iy))
    theta = np.arctan2(Iy, Ix)

    return math.degrees(np.mean(theta))


if __name__ == '__main__':
    print(mean_angle(sys.argv[1]))